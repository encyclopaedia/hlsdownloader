# hlsdownloader

This is a simple script to download hls streams from online sources. Works on any online stream where you have
a url or a downloaded file and want to download and merge streams into a video file.

## Usage:

### Options:

| switch  | example                                      | description |
|---------|----------------------------------------------|-------------|
| -h      |                                              | print help menu 
| -i      | -i http://example.com/sample.m3u8            | use the specified m3u8 file as input
| -i      | -i /tmp/input.m3u8                           | use the specified downloaded m3u8 file as input
| -o      | -o /tmp/output.mp4                           | once all chunks are downloaded, merge the chunks into this filename
| -u      | -u "https://example.com/playlist/"           | use this url as the prefix - used for relative m3u8 files. Useful to provide source url for downloaded files
| -H      | -H 'referer: https://example.com'            | additional headers to be passed to the requests. Can provide multiple headers with additional -H switches. Colon separated
| -v      | -v                                           | verbose logging to get info on what's happening currently. No param required
| -w      | -w 4                                         | number of parallel processes to download streams. helps download streams more efficiently

### Command line

1. copy `hlsdownloader` to a directory that has executables in it, like /usr/local/bin, /bin etc 
1. install python requirements by running `python3 -m pip install -r requirements.txt` 
1. use hlsdownloader as any other program with all the command line switches

### Docker
Docker image is built for multiple architectures. To use pre-built docker image, run this command (or create a function in your profile). Note: Update the version number accordingly
The advantage of using the docker image is you don't have to worry about installing dependencies and installing the full python suite to get the program working

```bash
docker run \
    --name hls \
    --rm \
    --user "$(id -u)":"$(id -g)" \
    -v "$PWD":/downloads \
    -w /downloads \
    docker.io/encyclopaedia/hlsdownloader:0-0-1 \
    hlsdownloader
```
