FROM python:3-alpine
ADD requirements.txt /
ADD hlsdownloader /bin/
RUN pip install -r /requirements.txt
RUN mkdir -p /downloads
WORKDIR /downloads
